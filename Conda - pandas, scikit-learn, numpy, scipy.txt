~~~~~
Goal: Install conda and use it to install numpy, pandas, scipy, scikit-learn
~~~~~

1. download anaconda or miniconda distribution online.
anaconda: https://www.continuum.io/downloads
miniconda: http://conda.pydata.org/miniconda.html

2. anaconda automatically downloads and installs all 150 packages but takes up to 3GB in memory, while miniconda initially installs only the necessary packages and lets you install packages individually.

optional: you can download the conda cheatsheet for guide in installing packages. http://conda.pydata.org/docs/using/cheatsheet.html

note: i've read in some that you should create an environment first before installing a package, else it uses the current active environment. i dont see/understand (yet) the use of building an environment first so i disregarded it. 

UPDATE: GETS KO NA PURPOSE NG ENVIRONMENT PAPI (ata). yung environment kasi, as its name says, parang "ENVIRONMENT". HAHAHA gets? for this case, conda creates an environment for python. so bale, doon nagrurun si python and dun din siya kumukuha ng libraries. when activating an environment in conda, it sets the python environment doon sa niactivate mong directory. IN OTHER WORDS, mapapalitan yung pythonpath mo i guess? kasi naging problema ko nung ininstall ko yung conda, is "nawala" yung previous python libraries ko. pero hindi talaga siya nawala, nakalagay kasi siya sa ibang directory/environment. so hindi niya lang nababasa kasi iba na yung path ni python.

i dont know pero ang hassle ni conda? see more: https://github.com/conda/conda/issues/448

$ conda create -n name_of_my_env python

$ source activate name_of_my_env

~~~~~

install pandas (it automatically installs all its dependencies including numpy)

$ conda install pandas

see more: http://pandas.pydata.org/pandas-docs/stable/install.html

~~~~~

install scikit-learn (it automatically installs all its dependencies including numpy)

$ conda install scikit-learn
